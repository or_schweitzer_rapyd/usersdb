import { error } from 'console';
import express from 'express';
import { Request,Response, NextFunction } from 'express';
import { UserDetails } from './globalTypes.js';
import HttpException from './Exceptions/Http.exception.js';
import { generateRequestID } from './index.js';
import { logger } from './Logger.js';
import UserNotFoundException from './Exceptions/UserNotFound.exception.js';

import * as userRepo from "./UserRepository.js";

const router = express.Router();
router.use(express.json())

async function findByIdElseThrow(id : string){
    const foundUser = await (userRepo.readUserByID(Number(id)));
    
        if(foundUser !== undefined){
            return foundUser;
        }else{
            throw new UserNotFoundException(Number(id))
        }
}


//Read all users 
router.get('/',async (req : Request,res :Response,next : NextFunction)=>{
    try{
        const allUsers = await (userRepo.readAllUsers());
        res.status(200).json(allUsers);

    }catch(e){
        next(e);
    }
    
})


//read user by ID 
router.get('/:userID',async (req : Request,res :Response, next : NextFunction)=>{
   try {
        const foundUser = await (findByIdElseThrow(req.params?.userID));
        res.status(200).json(foundUser);
       
   } catch (e) {
        next(e);
   } 
    
})

//create user 
router.post('/',async (req : Request,res :Response,next :NextFunction)=>{
    try {
        await (userRepo.save(req.body));
        res.status(200).send('user saved');
        
    } catch (e) {
        next(e);
    }
})


//update user 
router.put('/:userID',async (req : Request,res :Response,next :NextFunction)=>{
   try {
       
        const foundUser = await (findByIdElseThrow(req.params?.userID));
        await (userRepo.update({ID : Number(req.params.userID),...req.body}));
        res.status(200).send('user saved');
        
   } catch (e) {
        next(e);    
   }
   
})

//delete user
router.delete('/:userID',async (req : Request,res :Response,next:NextFunction)=>{
    try {
        const foundUser = await (findByIdElseThrow(req.params?.userID));
        await (userRepo.deleteUser(Number(req.params.userID)));
        res.status(200).send('user deleted');
       
        
    } catch(e) {
        next(e);
    }
  
})


export default router;
