import fs from 'fs/promises';
import { Request,Response, NextFunction } from 'express';
import HttpException from './Exceptions/Http.exception.js';

export const errLogger = (err:HttpException,req:Request, res:Response,next:NextFunction)=>{
    const date = new Date();
    const dateStr = `${date.toDateString()} ${date.toTimeString()}`;
    err.status = err.status || 500;
    fs.writeFile(`${process.cwd()}/myErrorlog.log`,`RequestID: ${req.myID} ,    Status Code: ${err.status} ,     on: ${dateStr},    message:${err.message},    stack:${err.stack} 
     \n`, {
        flag: "a"
    });
    
    res.status(err.status).json({message: err.message,
                          status : err.status });
}

