

export interface UserDetails {
    name : string;
}

export interface User {
    ID : number;
    name : string;
}

export interface DBData {
    users : User [];
    nextID : number;

}
