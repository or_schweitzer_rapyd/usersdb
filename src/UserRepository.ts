import fs from 'fs/promises'
import {User,UserDetails,DBData} from './globalTypes.js' 
import { getDataFromDB,writeToDB } from './Utils/UtilsDB.js';

const { PATH_DB ="./usersDB.JSON" } = process.env;


export async function readAllUsers() : Promise<User[]> {
    try {
        const {users}  = await getDataFromDB();
        return  users;
    } catch (e) {
       return [];
    }

}

export async function readUserByID(userID : number){
    const users = await readAllUsers()
    const foundUser = users.find((user: User) => user.ID === userID);
    
    return foundUser;
}

export async function save(userDetails : UserDetails) {
    const {users,nextID}  = await getDataFromDB();
    const newUser = {ID:nextID ,...userDetails}
    users.push(newUser)
    const resultData = {users, nextID : nextID + 1};
    await writeToDB(resultData);

    return;
    
}

export async function update(user : User){
    let {users,nextID}  = await getDataFromDB();
    users = users.filter((currUser: User) => currUser.ID !== user.ID);
    users.push(user)
    const resultData = {users, nextID};
    await writeToDB(resultData);

    return;
}

export async function deleteUser(userID : number){
    let {users,nextID}  = await getDataFromDB();
    users = users.filter((currUser: User) => (currUser.ID !== userID));
    const resultData = {users, nextID};
    await writeToDB(resultData);

    return;
}


