
import fs from 'fs/promises';
import { Request,Response, NextFunction } from 'express';
export const logger = (req:Request, res:Response, next:NextFunction)=>{
    const date = new Date();
    const dateStr = `${date.toDateString()} ${date.toTimeString()}`;
    fs.writeFile(`${process.cwd()}/mylog.log`, ` ID : ${req.myID} ${req.method}, ${req.originalUrl} on ${dateStr}\n`, {
        flag: "a"
    });
    next();
}