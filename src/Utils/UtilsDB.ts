import { DBData } from "./globalTypes.js";
import fs from 'fs/promises';


const { PATH_DB ="./usersDB.JSON" } = process.env;

export async function getDataFromDB() : Promise<DBData> {
    try {
        const rawData = await fs.readFile(PATH_DB, 'utf8')
        const jsonData = JSON.parse(rawData);
        return jsonData;
    } catch (e) {
       return {users : [],nextID : 0};
    }

}

export async function writeToDB(data : DBData){
    await fs.writeFile(PATH_DB, JSON.stringify(data, null, 2))
}