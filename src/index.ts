import express from 'express';
import { Request,Response, NextFunction } from 'express';
import log from '@ajar/marker';
import { logger } from './Logger.js';
import router from './UserController.js'
import * as utils from './Utils/Utils.js'
import { errLogger } from './ErrorHandler.js';
import HttpException from './Exceptions/Http.exception.js';
import UrlNotFoundException from './Exceptions/UrlNotFound.exception.js';
const { PORT, HOST = "localhost" } = process.env;




const app = express();
app.use(generateRequestID);
app.use(logger);

export function generateRequestID(req : Request, res : Response,next : NextFunction)  {
    req.myID = utils.generateID();
    next();
}


app.use('/users', router)

//not found
app.use((req : Request, res : Response,next:NextFunction) => {
    next(new UrlNotFoundException(req.originalUrl));
    
})

app.use(errLogger)
app.use(express.json())





app.listen(Number(PORT), HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

